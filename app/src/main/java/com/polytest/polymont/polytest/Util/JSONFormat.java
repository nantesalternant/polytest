package com.polytest.polymont.polytest.Util;

import com.polytest.polymont.polytest.Data.Candidat;
import com.polytest.polymont.polytest.Data.Quizz;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class JSONFormat {

    private Candidat candidat = new Candidat();
    private Quizz q  = new Quizz();

    public JSONFormat(Candidat candidat){

        this.candidat = candidat;
        this.q = candidat.getQuizz();
    }


    public JSONArray createJSON() throws JSONException {
        JSONArray jsonArray = new JSONArray();

        for(int i=0; i<q.size();i++){
            JSONObject phrase = new JSONObject();
            phrase.put("question", q.getQuestion(i).getEnonce());
            ArrayList<String> candidatRepsList = candidat.getPersonalResponses();
            phrase.put("personal-response", candidatRepsList.get(i));
            phrase.put("reps", q.getArrayOfRepsIntoJson(i));

            jsonArray.put(phrase);
        }


        return jsonArray;
    }
}
