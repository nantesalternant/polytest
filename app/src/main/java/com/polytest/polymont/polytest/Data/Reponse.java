package com.polytest.polymont.polytest.Data;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;

public class Reponse extends Phrase implements Serializable {

    private String enonce;
    private boolean correct = false;

    public Reponse(){super();}

    public Reponse(String enonce){
        super(enonce);
        this.enonce = enonce;
    }

    public Reponse(String enonce, boolean correct){
        super(enonce);
        this.enonce = enonce;
        this.correct = correct;
    }

    public String getEnonce() {
        return enonce;
    }

    public void setEnonce(String enonce) {
        this.enonce = enonce;
    }

    public boolean isCorrect() {
        return correct;
    }

    public void setCorrect(boolean correct) {
        this.correct = correct;
    }

    JSONObject getJSONObject() {
        JSONObject obj = new JSONObject();
        try {
            obj.put("enonce", enonce);
            obj.put("correct", correct);
        } catch (JSONException e) {
            e.getMessage();
        }
        return obj;
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
