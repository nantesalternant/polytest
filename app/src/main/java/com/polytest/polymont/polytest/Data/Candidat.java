package com.polytest.polymont.polytest.Data;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by Zaccharie.BOUVY on 02/01/2017.
 */

public class Candidat implements Serializable {

    private int id;
    private int score;
    private String nom;
    private Calendar dateTest;
    private Timestamp durationTest;
    private Calendar durationCal;
    private Quizz quizz;
    private ArrayList<String> personalReps = new ArrayList<>();


    public Candidat(){}

    public Candidat(String nom){
        this.nom = nom;
    }

    public Candidat(String nom, int score){
        this.nom = nom;
        this.score = score;
    }

    public Candidat(String nom, int score, Quizz quizz){
        this.nom = nom;
        this.score = score;
        this.quizz = quizz;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }


    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Calendar getDateTest() {
        return dateTest;
    }

    public void setDateTest(Calendar dateTest) {
        this.dateTest = dateTest;
    }

    public Calendar getDurationTest() {
        return durationCal;
    }

    public void setDurationTest(Calendar durationTest) {
        this.durationCal = durationTest;
    }
    public void setDurationTestTS(Timestamp durationTest) {
        this.durationTest = durationTest;
    }

    public Timestamp getDurationTestTS() {
        return durationTest;
    }

    public Quizz getQuizz() {
        return quizz;
    }

    public void setQuizz(Quizz quizz) {
        this.quizz = quizz;
    }

    public void addPersonalRep(String value){
        personalReps.add(value);
    }

    public ArrayList<String> getPersonalResponses(){
        return personalReps;
    }

}
