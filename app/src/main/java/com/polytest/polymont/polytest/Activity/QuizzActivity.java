package com.polytest.polymont.polytest.Activity;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.polytest.polymont.polytest.Data.Candidat;
import com.polytest.polymont.polytest.Data.Question;
import com.polytest.polymont.polytest.Data.Quizz;
import com.polytest.polymont.polytest.Data.Reponse;
import com.polytest.polymont.polytest.R;
import com.polytest.polymont.polytest.Util.Excel;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.concurrent.TimeUnit;

import es.dmoral.toasty.Toasty;

/**
 * Created by Zaccharie.BOUVY on 02/01/2017.
 */
public class QuizzActivity extends Activity implements View.OnClickListener{

    private com.polytest.polymont.polytest.Data.Candidat candidat;
    private CheckBox reponse1, reponse2, reponse3, reponse4;
    private TextView questionNum, questionTexte;
    private Button btnSuivant;
    private ProgressBar progressBar;
    private int itrQuestion = 0;
    private Quizz quizz = new Quizz();
    private LinkedHashMap<Question, ArrayList<Reponse>> randomMap = new LinkedHashMap<>();
    private LinkedHashMap<Question, ArrayList<Reponse>> totalQuizzMap = new LinkedHashMap<>();
    private ArrayList<Reponse> reponses = new ArrayList<>();
    private boolean itsCorrect = false;
    private int score;
    private final int NUMBER_OF_QUESTION = 20;
    private int forcePassage;
    private Calendar startCal;
    private Calendar endCal;
    private long duration;
    private String testName = "";
    private String testLevel = "";
    private TextView tvTestName;
    private SimpleDateFormat formatDateDuration = new SimpleDateFormat("mm:ss");
    private Quizz quizzForNoneReps = new Quizz();
    private boolean isBonus;
    private ArrayList<String> personalResponses = new ArrayList<>();
    private int sizeNewQuizz = 0;
    private Handler handler = new Handler();
    private Runnable runToast;
    private ScrollView scrollView;
    private ImageView blueArrow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quizz);

        startCal = Calendar.getInstance();

        //force le mode paysage
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        isBonus = getIntent().getExtras().getBoolean("isBonus");
        candidat = (Candidat) getIntent().getSerializableExtra("candidat");
        testName =  getIntent().getStringExtra("testName");
        testLevel =  getIntent().getStringExtra("testLevel");

        reponse1 = (CheckBox)findViewById(R.id.reponse1);
        reponse2 = (CheckBox)findViewById(R.id.reponse2);
        reponse3 = (CheckBox)findViewById(R.id.reponse3);
        reponse4 = (CheckBox)findViewById(R.id.reponse4);

        scrollView = (ScrollView) findViewById(R.id.scroller);
        blueArrow = (ImageView) findViewById(R.id.flechbleu);
        questionNum = (TextView) findViewById(R.id.question_num);
        questionTexte = (TextView) findViewById(R.id.question_phrase);
        tvTestName = (TextView) findViewById(R.id.test_name_quizz);
        btnSuivant = (Button) findViewById(R.id.btn_submit);
        progressBar = (ProgressBar) findViewById(R.id.progressbar);

        testLevel = testLevel.toLowerCase();
        switch (testLevel){
            case Excel.NIVEAU_JUNIOR:
                tvTestName.setText("Test en cours : "+testName+" JUNIOR");
                break;
            case Excel.NIVEAU_EXPERT:
                tvTestName.setText("Test en cours : "+testName+" EXPERT");
                break;
        }


        forcePassage = 0;
        btnSuivant.setOnClickListener(this);

        randomMap.clear();
        randomMap = randomizeQuizz(Excel.quizz);

        quizz.setQuizzMap(randomMap);
        quizz.setTestName(testName);
        quizz.setTestLevel(testLevel);
        candidat.setQuizz(quizz);

        progressBar.setIndeterminate(false);
        progressBar.incrementProgressBy(1);
        progressBar.setProgress(1);
        progressBar.setMax(quizz.size());

        updateQuestion(itrQuestion);

    }



    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_submit:

                //Si on force le passage à la question suivante
                if (this.onceChecked() || forcePassage >= 1 || isBonus){


                    forcePassage = 0;
                    if (this.checkIfCorrect(itrQuestion)) {
                        candidat.setScore(++score);
                    }

                    //PREPARATION NOUVELLE QUESTION
                    itrQuestion++;

                    //FIN DU TEST OU BONUS
                    if (itrQuestion >= quizz.size() || isBonus) {


                        if(!isBonus) {
                            personalResponses = candidat.getPersonalResponses();
                            sizeNewQuizz = Collections.frequency(personalResponses, "0");
                            if(sizeNewQuizz>0){
                                 runToast = new Runnable() {
                                    public void run() {
                                        Toasty.warning(QuizzActivity.this, getString(R.string.qstBonus), Toast.LENGTH_SHORT, true).show();
                                        handler.postDelayed(this, 3000);
                                    }
                                };

                                handler.post(runToast);
                            }
                        }

                        //si certaines questions sont non repondu
                        if(personalResponses.contains("0")){
                            sizeNewQuizz = Collections.frequency(personalResponses, "0");
                            isBonus = true;
                            for(int i= 0; i<personalResponses.size();i++){
                                if(personalResponses.get(i).equals("0")){
                                    this.clearAllCheck();
                                    resetResponsesfield();
                                    updateQuestion(i);
                                    itrQuestion = i;
                                    questionNum.setTextColor(Color.rgb(230,0,0));
                                    questionNum.setTypeface(null, Typeface.BOLD);

                                    break;
                                }
                            }
                            //ultima question
                            if(sizeNewQuizz ==1){
                                btnSuivant.setText(R.string.terminer);
                            }

                        }

                        else{

                            handler.removeCallbacks(runToast);

                            endCal = Calendar.getInstance();
                            duration = endCal.getTimeInMillis() - startCal.getTimeInMillis();

                            long minutes = TimeUnit.MILLISECONDS.toMinutes(duration);
                            long seconds = TimeUnit.MILLISECONDS.toSeconds(duration);

                            try {
                                long total = formatDateDuration.parse(minutes + ":" + seconds).getTime();
                                Timestamp ts = new Timestamp(total);

                                candidat.setDurationTestTS(ts);

                            } catch (ParseException e) {
                                e.printStackTrace();
                            }

                            Intent i = new Intent(QuizzActivity.this, ScoreActivity.class);
                            i.putExtra("candidat", candidat);
                            startActivity(i);
                        }
                    }
                    else {
                        resetResponsesfield();
                        progressBar.setProgress(progressBar.getProgress() + 1);
                        this.clearAllCheck();
                        this.updateQuestion(itrQuestion);

                        if (itrQuestion == quizz.size() - 1 && !candidat.getPersonalResponses().contains("0")) {
                            btnSuivant.setText(R.string.terminer);
                        }
                    }
                }
                else {
                        forcePassage++;
                }

                break;
        }
    }

    private void resetResponsesfield(){
        reponse1.setVisibility(View.VISIBLE);
        reponse2.setVisibility(View.VISIBLE);
        reponse3.setVisibility(View.VISIBLE);
        reponse4.setVisibility(View.VISIBLE);

        reponse1.setText("");
        reponse2.setText("");
        reponse3.setText("");
        reponse4.setText("");
    }

    private boolean onceChecked() {
        if(!(reponse1.isChecked() || reponse2.isChecked() || reponse3.isChecked() || reponse4.isChecked())) {
            if(!isBonus) {
                if (forcePassage == 0)
                    Toasty.info(this, getString(R.string.toast_force), Toast.LENGTH_LONG, true).show();
            }
            return false;
        }
        else {
            return true;
        }
    }

    private void updateQuestion(int iterator) {
        ArrayList<Reponse> reponses = quizz.getReponses(iterator);
        questionNum.setText("Question "+(iterator+1)+"/"+quizz.size()+" :");
        questionTexte.setText(quizz.getQuestion(iterator).getEnonce());
        if(!reponses.get(0).getEnonce().equals(" "))
        {
            reponse1.setText(reponses.get(0).getEnonce());
        }
        else
        {
            reponse1.setVisibility(View.INVISIBLE);
        }

        if(!reponses.get(1).getEnonce().equals(" "))
        {
            reponse2.setText(reponses.get(1).getEnonce());
        }
        else
        {
            reponse2.setVisibility(View.INVISIBLE);
        }
        if(!reponses.get(2).getEnonce().equals(" "))
        {
            reponse3.setText(reponses.get(2).getEnonce());
        }
        else
        {
            reponse3.setVisibility(View.INVISIBLE);
        }
        if(!reponses.get(3).getEnonce().equals(" "))
        {
            reponse4.setText(reponses.get(3).getEnonce());
        }
        else
        {
            reponse4.setVisibility(View.INVISIBLE);
        }

        //detection de l'affichage de la fleche d'aide bleu
        ViewTreeObserver observer = scrollView.getViewTreeObserver();
        observer.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                int viewHeight = scrollView.getMeasuredHeight();
                int contentHeight = scrollView.getChildAt(0).getHeight();
                if (viewHeight - contentHeight < 0) {
                    blueArrow.setVisibility(View.VISIBLE);
                    blueArrow.setImageResource(R.drawable.animation_arrow);
                    AnimationDrawable frameAnimation = (AnimationDrawable) blueArrow.getDrawable();
                    frameAnimation.start();
                } else {
                    blueArrow.setVisibility(View.INVISIBLE);
                }
            }
        });
    }

    private void clearAllCheck(){
        reponse1.setChecked(false);
        reponse2.setChecked(false);
        reponse3.setChecked(false);
        reponse4.setChecked(false);
    }

    public boolean noneReponsesChecked()
    {
        return !reponse1.isChecked() && !reponse2.isChecked() && !reponse3.isChecked() && !reponse4.isChecked();
    }

    private boolean checkIfCorrect(int itrQuestion){
        reponses = quizz.getReponses(itrQuestion);

        itsCorrect = reponse1.isChecked() == reponses.get(0).isCorrect() &&
                reponse2.isChecked() == reponses.get(1).isCorrect() &&
                reponse3.isChecked() == reponses.get(2).isCorrect() &&
                reponse4.isChecked() == reponses.get(3).isCorrect();

        if(isBonus){
            if(noneReponsesChecked()){
                personalResponses.set(itrQuestion,"false");
            }
            else{
                personalResponses.set(itrQuestion,getResponsesCandidate());
            }
        }
        else{
            candidat.addPersonalRep(getResponsesCandidate());
        }

        return itsCorrect;
    }

    private LinkedHashMap<Question, ArrayList<Reponse>> randomizeQuizz(Quizz quizz) {

        LinkedHashMap<Question, ArrayList<Reponse>> randomMap = new LinkedHashMap<>();
        ArrayList<Question> questions = new ArrayList<>();
        LinkedHashMap<Question, ArrayList<Reponse>> quizzMap = quizz.getQuizzMap();

        /*ON AJOUTE AUTANT DE QUESTIONS QUE NECESSAIRE SI
          LE NOMBRE DE QUESTIONS CORRESPONDANTES AU NIVEAU N'EST PAS SUFFISANT
          ON PREND LE NIVEAU INFERIEUR QUAND EN PRIORITE LORSQUE C'EST POSSIBLE
          SINON ON AJOUTE AVEC LES QUESTIONS QU'ON POSSEDE
          */
        switch (testLevel){
            case Excel.NIVEAU_JUNIOR:
                questions.addAll(Excel.juniorQ);
                break;

            case Excel.NIVEAU_EXPERT:
                questions.addAll(Excel.expertQ);
                break;
        }

        Collections.shuffle(questions);

        if(questions.size()<NUMBER_OF_QUESTION){
            for(int i=0; i < questions.size(); i++){
                randomMap.put(questions.get(i), quizzMap.get(questions.get(i)));
            }
        }
        else{
            for(int i=0; i < NUMBER_OF_QUESTION; i++){
                randomMap.put(questions.get(i), quizzMap.get(questions.get(i)));
            }
        }


        return randomMap;
    }

    @Override
    public void onBackPressed() {

        Toasty.info(this, getString(R.string.toast_back), Toast.LENGTH_SHORT, true).show();

    }

    public String getResponsesCandidate() {
        StringBuilder str = new StringBuilder();

        if(reponse1.isChecked()){
            str.append("1 ");
        }
        if(reponse2.isChecked()){
            str.append("2 ");
        }
        if(reponse3.isChecked()){
            str.append("3 ");
        }
        if(reponse4.isChecked()){
            str.append("4 ");
        }
        if(noneReponsesChecked()){
            str.append("0");
        }

        return str.toString();
    }
}
