package com.polytest.polymont.polytest.Activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.animation.OvershootInterpolator;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.hookedonplay.decoviewlib.DecoView;
import com.hookedonplay.decoviewlib.charts.SeriesItem;
import com.hookedonplay.decoviewlib.events.DecoEvent;
import com.polytest.polymont.polytest.Data.Candidat;
import com.polytest.polymont.polytest.R;

import es.dmoral.toasty.Toasty;

/**
 * Created by Zaccharie.BOUVY on 02/01/2017.
 */
public class ScoreActivity extends Activity implements View.OnClickListener{

    private Candidat candidat;
    private TextView score;
    private DecoView decoView;
    private Button regenerateBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_score);

        //force le mode paysage
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        candidat = (Candidat) getIntent().getSerializableExtra("candidat");

        score = (TextView) findViewById(R.id.score);
        decoView = (DecoView) findViewById(R.id.dynamicScore);
        regenerateBtn = (Button) findViewById(R.id.btn_home);

        regenerateBtn.setOnClickListener(this);

        arcCreation(decoView,score);
    }

    @Override
    public void onBackPressed() {
        Toasty.warning(this, getString(R.string.toast_back), Toast.LENGTH_SHORT, true).show();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_home:
                Intent intent = new Intent(ScoreActivity.this,MainActivity.class);
                startActivity(intent);
                break;
        }
    }


    //Méthode permettant de créer un cercle
    public void arcCreation(DecoView arcSelected, final TextView txtPourcent) {
        // Create background track
        arcSelected.addSeries(new SeriesItem.Builder(ContextCompat.getColor(arcSelected.getContext(),R.color.colorPrimaryNoirPolymont))
                .setRange(0, 100, 100)
                .setInitialVisibility(false)
                .setLineWidth(32f)
                .build());

        //Create data series track
        final SeriesItem seriesItem1 = new SeriesItem.Builder(ContextCompat.getColor(arcSelected.getContext(),R.color.colorPrimaryBleuPolymont))//Color.argb(255, 69, 124, 191))
                .setRange(0, 100, 0)
                .setInterpolator(new OvershootInterpolator())//applique un effet de "bounce" sur le graph
                .setLineWidth(32f)//epaisseur du trait
                .setSpinDuration(2500)//vitesse d'affichage
                .setSpinClockwise(true)//sens d'affichage du graphe
                .setCapRounded(true)//arrondi les bouts
                .setChartStyle(SeriesItem.ChartStyle.STYLE_DONUT)//met le style en tant que cercle
                //.setInset(new PointF(20f, 20f))//permet de décaler les points ensemble
                .build();

        int series1Index = arcSelected.addSeries(seriesItem1);

        arcSelected.addEvent(new DecoEvent.Builder(DecoEvent.EventType.EVENT_SHOW, true)
                .setDelay(1000)
                .setDuration(2000)
                .build());

        seriesItem1.addArcSeriesItemListener(new SeriesItem.SeriesItemListener() {
            @SuppressLint("DefaultLocale")
            @Override
            public void onSeriesItemAnimationProgress(float percentComplete, float currentPosition) {
                float percentFilled = ((currentPosition - seriesItem1.getMinValue()) / (seriesItem1.getMaxValue() - seriesItem1.getMinValue()));
                txtPourcent.setText(String.format("%.0f%%", percentFilled * 100f));
            }

            @Override
            public void onSeriesItemDisplayProgress(float percentComplete) {

            }
        });

        com.polytest.polymont.polytest.Bdd.Candidat candidatBdd = new com.polytest.polymont.polytest.Bdd.Candidat(this);
        candidatBdd.insertCandidat(candidat);

        System.out.println("checking value SCORE END "+candidat.getScore()+" percentage "+candidat.getScore()/0.2f);
        float res = candidat.getScore()/0.2f;  //on met le score en pourcentage
        arcSelected.addEvent(new DecoEvent.Builder(res).setIndex(series1Index).setDelay(1000).build());
    }


}