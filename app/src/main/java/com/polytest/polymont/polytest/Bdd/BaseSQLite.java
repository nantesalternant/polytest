package com.polytest.polymont.polytest.Bdd;

/**
 * Created by Zaccharie.BOUVY on 17/01/2017.
 */
import android.annotation.SuppressLint;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


class BaseSQLite extends SQLiteOpenHelper{

    // ici ont déclare le nom de la table évent et de ses attribut
    private static final String TABLE_CANDIDAT = "Candidat";
    private static final String COL_ID = "id";
    private static final String COL_NAME = "name";
    private static final String COL_SCORE = "score";
    private static final String COL_DATE_TEST = "date";
    private static final String COL_DURATION_TEST = "duration";
    private static final String COL_TEST_DONE = "test";
    private static final String COL_TEST_LEVEL = "level";
    private static final String COL_FK_QUIZZ = "leQuizz";


    // Préparation des requete SQL
    private static final String CREATE_BDD_CANDIDAT =
            "CREATE TABLE " + TABLE_CANDIDAT + " ("
                    + COL_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + COL_NAME + " STRING NOT NULL, "
                    + COL_SCORE + " INTEGER NOT NULL, "
                    + COL_DATE_TEST + " DATETIME NOT NULL, "
                    + COL_DURATION_TEST + " DATETIME NOT NULL, "
                    + COL_TEST_DONE +" STRING NOT NULL, "
                    + COL_TEST_LEVEL +" STRING NOT NULL, "
                    + COL_FK_QUIZZ + " STRING NOT NULL);"
            ;


    @SuppressLint("SQLiteString")
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_BDD_CANDIDAT);
    }


    BaseSQLite(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + CREATE_BDD_CANDIDAT);
        onCreate(db);
    }
}
