package com.polytest.polymont.polytest.Activity;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;

import android.support.v4.app.ActivityCompat;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.polytest.polymont.polytest.Data.Candidat;
import com.polytest.polymont.polytest.R;
import com.polytest.polymont.polytest.Util.Excel;
import com.polytest.polymont.polytest.Util.FileSinPath;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import es.dmoral.toasty.Toasty;

public class MainActivity extends Activity implements View.OnClickListener, RadioGroup.OnCheckedChangeListener {

    private static final String MDP = "novia";
    private Button btnSubmit;
    private ImageButton btnAdmin;
    private EditText edCandidat;
    private String nameCandidat;
    private Spinner spinnerDataChoose;
    private FileSinPath data = new FileSinPath(System.getenv("EXTERNAL_STORAGE")+"/QuizzApp/");
    private String filePath = "/QuizzApp/";
    private Point p;
    private String testName= "";
    private String testLevel = Excel.NIVEAU_JUNIOR;
    private RadioGroup groupLevel;
    private TextView xperienceField;

    // Storage Permissions
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //force le mode paysage
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        btnSubmit = (Button) findViewById(R.id.btn_submit);
        btnAdmin = (ImageButton) findViewById(R.id.btn_access_admin);
        edCandidat = (EditText) findViewById(R.id.ed_candidat);
        spinnerDataChoose = (Spinner) findViewById(R.id.spinnerChoixData);
        groupLevel = (RadioGroup) findViewById(R.id.radiogroup_level);
        xperienceField = (TextView) findViewById(R.id.xperience);

        btnSubmit.setOnClickListener(this);
        btnAdmin.setOnClickListener(this);
        groupLevel.setOnCheckedChangeListener(this);

        edCandidat.requestFocus();

        verifyStoragePermissions(this);

        //on remplit le spinner qui parcours le dossier QuizzApp qui contient les datas
        fillDataForSpinner();

    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        View radioButton = group.findViewById(checkedId);
        int index = group.indexOfChild(radioButton);

        switch (index) {
            case 0:
                testLevel = Excel.NIVEAU_JUNIOR;
                break;
            case 1:
                testLevel = Excel.NIVEAU_EXPERT;
                break;
        }
    }

    private void fillDataForSpinner() {

        List<FileSinPath> list = new ArrayList<>();

        Collections.addAll(list, data.listFiles());

        final ArrayAdapter<FileSinPath> filenameAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_dropdown_item_1line, list);



        spinnerDataChoose.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                filePath = "/QuizzApp/"+spinnerDataChoose.getSelectedItem().toString()+".xls";

                testName = spinnerDataChoose.getSelectedItem().toString();

                Excel.readExcelFile(MainActivity.this,filePath);

                //On permet içi de vérifier si on possède assez de questions expertes pour autoriser le démarrage d'un test
                View radioExpert = groupLevel.findViewById(R.id.radio_expert);
                View radioJunior = groupLevel.findViewById(R.id.radio_junior);
                if(Excel.expertQ.size() < 20){
                    xperienceField.setVisibility(View.INVISIBLE);
                    radioExpert.setVisibility(View.INVISIBLE);
                    testLevel = Excel.NIVEAU_JUNIOR;
                    radioJunior.setVisibility(View.INVISIBLE);
                }
                else
                {
                    xperienceField.setVisibility(View.VISIBLE);
                    radioJunior.setVisibility(View.VISIBLE);
                    radioExpert.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spinnerDataChoose.setAdapter(filenameAdapter);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_submit:
                nameCandidat = edCandidat.getText().toString();

                if(nameCandidat.equals("")){
                    edCandidat.requestFocus();
                    Toasty.info(this, getString(R.string.inputname), Toast.LENGTH_SHORT, true).show();
                }
                else if((testLevel.equals(Excel.NIVEAU_EXPERT) && Excel.expertQ.size()==0) ||
                        testLevel.equals(Excel.NIVEAU_JUNIOR) && Excel.juniorQ.size()==0){
                    Toasty.error(this, "Le test ne peut être chargé\nMotif: Aucune question trouvée", Toast.LENGTH_LONG, true).show();
                }
                else{
                    Intent i = new Intent(MainActivity.this, QuizzActivity.class);
                    i.putExtra("candidat", new Candidat(nameCandidat));
                    i.putExtra("testName", testName);
                    i.putExtra("testLevel", testLevel);
                    startActivity(i);
                }

                break;
            case R.id.btn_access_admin:

                this.showPopupMDP(this, p);

                break;
        }
    }

    @Override
    public void onBackPressed() {
        Toasty.info(this, getString(R.string.toast_back), Toast.LENGTH_SHORT, true).show();

    }

    private void showPopupMDP(final MainActivity context, Point mPoint) {
        int OFFSET_X = 60;
        int OFFSET_Y = 70;


        // Inflate the popup_layout.xml
        LinearLayout viewGroup = (LinearLayout) context.findViewById(R.id.popup);
        LayoutInflater layoutInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = layoutInflater.inflate(R.layout.popupadmin, viewGroup);

        // Creating the PopupWindow
        final PopupWindow popup = new PopupWindow(context);
        popup.setContentView(layout);
        popup.setWidth((2*mPoint.x / 5) + OFFSET_X);
        popup.setHeight(((2*mPoint.y / 11) - OFFSET_Y));
        popup.setFocusable(true);



        popup.setBackgroundDrawable(new BitmapDrawable());
        popup.showAtLocation(layout, 0, (mPoint.y-(7*mPoint.y/20)), (2*mPoint.x/ 7));



        final EditText enteredMDP = (EditText) layout.findViewById(R.id.ed_mdp);
        Button annuler = (Button) layout.findViewById(R.id.popup_annuler);
        annuler.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popup.dismiss();
            }
        });

        Button valider = (Button) layout.findViewById(R.id.popup_groupe_valider);
        valider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(enteredMDP.getText().toString().trim().equals(MDP)) {
                    enteredMDP.setText("");
                    Intent myIntent = new Intent(MainActivity.this, AdministrationActivity.class);
                    startActivity(myIntent);
                }
                else
                {
                    if(enteredMDP.getText().toString().equals("")){
                        Toasty.error(MainActivity.this, "Veuillez indiquer le mot de passe", Toast.LENGTH_SHORT, true).show();
                    }
                    else{
                        Toasty.error(MainActivity.this, "Mot de passe incorrect", Toast.LENGTH_SHORT, true).show();
                    }
                    popup.dismiss();
                }
            }
        });

    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {

        super.onWindowFocusChanged(hasFocus);

        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);

        p = new Point();
        p.x = metrics.heightPixels;
        p.y = metrics.widthPixels;

    }


    public void verifyStoragePermissions(Activity activity) {
        // Check if we have write permission
        int permission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permission != PackageManager.PERMISSION_GRANTED) {
            // We don't have permission so prompt the user
            ActivityCompat.requestPermissions(
                    activity,
                    PERMISSIONS_STORAGE,
                    REQUEST_EXTERNAL_STORAGE
            );
            verifyStoragePermissions(activity);
        }
    }
}
