package com.polytest.polymont.polytest.Data;

import java.io.Serializable;

/**
 * Created by Zaccharie.BOUVY on 02/01/2017.
 */

public class Question extends Phrase implements Serializable {

    private int id;
    private String enonce;

    public Question(){super();}

    public Question(int id, String enonce){
        super(enonce);
        this.id = id;
        this.enonce = enonce;
    }

    public Question(String enonce){
        super(enonce);
        this.enonce = enonce;
    }

    public String getEnonce() {
        return enonce;
    }

    public int getId() {
        return id;
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
