package com.polytest.polymont.polytest.Data;

import java.io.Serializable;

/**
 * Created by Zaccharie.BOUVY on 18/01/2017.
 */
public class Phrase implements Serializable {

    private String enonce;

    Phrase(String enonce) {
        this.enonce = enonce;
    }

    Phrase(){}

    public String getEnonce() {
        return enonce;
    }

    @Override
    public String toString() {
        return enonce;
    }
}
