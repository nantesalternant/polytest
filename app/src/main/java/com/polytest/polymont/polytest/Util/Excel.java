package com.polytest.polymont.polytest.Util;

import android.content.Context;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import com.polytest.polymont.polytest.Data.Question;
import com.polytest.polymont.polytest.Data.Quizz;
import com.polytest.polymont.polytest.Data.Reponse;
import com.polytest.polymont.polytest.R;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.CellType;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Iterator;

import es.dmoral.toasty.Toasty;


public class Excel {

    public static String TAG = "Excel works";
    public static Quizz quizz = new Quizz();
    public static int Cellposition = 0;
    private static Question question;
    private static Reponse reponse1, reponse2, reponse3, reponse4;
    private static final int ID_QUESTION = 0;
    private static final int ENONCE_QUESTION = 1;
    private static final int REPONSE_1 = 2;
    private static final int REPONSE_2 = 3;
    private static final int REPONSE_3 = 4;
    private static final int REPONSE_4 = 5;
    private static final int REPONSES_CORRECTES = 6;
    private static final int NIVEAU_DIFFICULTE = 7;
    public static final String NIVEAU_JUNIOR = "j";
    public static final String NIVEAU_EXPERT = "e";
    public static ArrayList<Question> juniorQ = new ArrayList<>();
    public static ArrayList<Question> expertQ = new ArrayList<>();


    public static void readExcelFile(Context context, String filename) {
        juniorQ.clear();
        expertQ.clear();

        if (!isExternalStorageWritable() || !isExternalStorageReadable())
        {

            Toasty.info(context, "Storage not available or read only", Toast.LENGTH_SHORT, true).show();
            return;
        }

        try{
            // Creating Input Stream
            //File file = new File(context.getExternalFilesDir(null), filename);
            File file = new File(Environment.getExternalStorageDirectory()+ filename);

            FileInputStream myInput = new FileInputStream(file);

            // Create a POIFSFileSystem object
            POIFSFileSystem myFileSystem = new POIFSFileSystem(myInput);

            // Create a workbook using the File System
            HSSFWorkbook myWorkBook = new HSSFWorkbook(myFileSystem);

            // Get the first sheet from workbook
            HSSFSheet mySheet = myWorkBook.getSheetAt(0);

            /** We now need something to iterate through the cells.**/
            Iterator rowIter = mySheet.rowIterator();
            //pour enlever la lecture de l'entête
            rowIter.next();

            quizz.clear();

            while(rowIter.hasNext()){
                HSSFRow myRow = (HSSFRow) rowIter.next();
                Iterator cellIter = myRow.cellIterator();
                while(cellIter.hasNext()){

                    HSSFCell myCell = (HSSFCell) cellIter.next();
                    myCell.setCellType(CellType.STRING);
                    //if(Integer.parseInt(myCell.toString())){Integer.parseInt(myCell.getStringCellValue())};
                    fillQuizz(Cellposition, myCell.toString());
                    Cellposition++;
                }
                Cellposition = 0;
            }
        }
        catch (Exception e){e.printStackTrace(); }


    }

    private static void fillQuizz(int positionCell, String cellValue) {
        switch (positionCell){
            case ID_QUESTION:

                break;

            case ENONCE_QUESTION:
                question = new Question(cellValue);
                break;

            case REPONSE_1:
                if(!cellValue.equals("")) {
                    reponse1 = new Reponse(cellValue);
                }
                else
                {
                    cellValue = " ";
                    reponse1 = new Reponse(cellValue);
                }
                break;

            case REPONSE_2:
                if(!cellValue.equals("")) {
                    reponse2 = new Reponse(cellValue);
                }
                else
                {
                    cellValue = " ";
                    reponse2 = new Reponse(cellValue);
                }
                break;

            case REPONSE_3:
                if(!cellValue.equals("")) {
                    reponse3 = new Reponse(cellValue);
                }
                else
                {
                    cellValue = " ";
                    reponse3 = new Reponse(cellValue);
                }
                break;

            case REPONSE_4:
                if(!cellValue.equals("")) {
                    reponse4 = new Reponse(cellValue);
                }
                else
                {
                    cellValue = " ";
                    reponse4 = new Reponse(cellValue);
                }
                break;

            case REPONSES_CORRECTES:
                String[] correctTab = cellValue.split("[,.]");
                ArrayList<Reponse> reponses = new ArrayList<>();

                reponses.add(reponse1);
                reponses.add(reponse2);
                reponses.add(reponse3);
                reponses.add(reponse4);

                for (String aCorrectRep : correctTab) {
                    int positionReponseCorrect = Integer.parseInt(aCorrectRep.trim()) - 1;

                    if (positionReponseCorrect != -1) {
                        reponses.get(positionReponseCorrect).setCorrect(true);
                    }
                }

                quizz.addQuestion(question, reponses);
            break;

            case NIVEAU_DIFFICULTE:
                cellValue = cellValue.toLowerCase();
                if(cellValue.equals(NIVEAU_JUNIOR)){
                    juniorQ.add(question);
                }
                else{
                    expertQ.add(question);
                }
                break;
        }


    }

    /* Checks if external storage is available for read and write */
    private static boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }

    /* Checks if external storage is available to at least read */
    private static boolean isExternalStorageReadable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state) ||
                Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            return true;
        }
        return false;
    }

}
