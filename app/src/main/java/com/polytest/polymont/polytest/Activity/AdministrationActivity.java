package com.polytest.polymont.polytest.Activity;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ImageButton;
import android.widget.Toast;

import com.polytest.polymont.polytest.Adapter.ExpandableListAdapter;
import com.polytest.polymont.polytest.Bdd.Candidat;
import com.polytest.polymont.polytest.Data.Phrase;
import com.polytest.polymont.polytest.Data.Question;
import com.polytest.polymont.polytest.Data.Quizz;
import com.polytest.polymont.polytest.Data.Reponse;
import com.polytest.polymont.polytest.R;

import java.net.URL;
import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
 * Created by yanis.dando on 17/01/2017.
 */
public class AdministrationActivity extends Activity{

    private Candidat candidat_bdd;
    private ArrayList<com.polytest.polymont.polytest.Data.Candidat> candidats = new ArrayList<>();
    private ExpandableListAdapter listAdapter;
    private ExpandableListView expListView;
    private LinkedHashMap<com.polytest.polymont.polytest.Data.Candidat, ArrayList<Phrase>> mapCdtPhrase = new LinkedHashMap<>();
    private ArrayList<String> personalReps = new ArrayList<>();
    private ImageButton resetTable;

    @Override
    public void onBackPressed() {
        Intent i = new Intent(this, MainActivity.class);
        startActivity(i);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin);

        //force le mode paysage
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        expListView = (ExpandableListView) findViewById(R.id.listeparticipants);
        resetTable = (ImageButton) findViewById(R.id.imgreset);

        candidat_bdd = new Candidat(this);
        candidats = candidat_bdd.getAllCandidat();

        prepareData();

        resetTable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog dialog = new AlertDialog.Builder(AdministrationActivity.this).create();
                dialog.setTitle("Attention");
                dialog.setMessage("Voulez-vous réinitialiser la base de données?");
                dialog.setButton(AlertDialog.BUTTON_NEGATIVE,"Non",new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });
                dialog.setButton(AlertDialog.BUTTON_POSITIVE,"Oui", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                candidat_bdd.resetDataBase();
                                Toast.makeText(AdministrationActivity.this, "Base de données nettoyée!", Toast.LENGTH_SHORT).show();
                                finish();
                            }
                        });
                dialog.show();
            }
        });
    }

    private void prepareData() {

        for(int i=0;i<candidats.size();i++){
            ArrayList<Phrase> phrases = new ArrayList<>();

            com.polytest.polymont.polytest.Data.Candidat currentCdt = candidats.get(i);

            Quizz quizz = currentCdt.getQuizz();
            //on affecte les reponses données par le candidat
            personalReps = currentCdt.getPersonalResponses();


            for(int j=0;j<quizz.size();j++){

                ArrayList<Reponse> currentReps = quizz.getReponses(j);
                Question currentQt = quizz.getQuestion(j);

                phrases.add(currentQt);
                phrases.addAll(currentReps);
            }

            mapCdtPhrase.put(currentCdt, phrases);
        }


        listAdapter = new ExpandableListAdapter(AdministrationActivity.this,candidats,mapCdtPhrase, personalReps);

        // setting list adapter
        expListView.setAdapter(listAdapter);

    }


}
