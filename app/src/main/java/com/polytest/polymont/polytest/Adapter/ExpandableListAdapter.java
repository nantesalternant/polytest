package com.polytest.polymont.polytest.Adapter;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v4.content.res.ResourcesCompat;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.polytest.polymont.polytest.Data.Candidat;
import com.polytest.polymont.polytest.Data.Phrase;
import com.polytest.polymont.polytest.Data.Question;
import com.polytest.polymont.polytest.Data.Reponse;
import com.polytest.polymont.polytest.R;
import com.polytest.polymont.polytest.Util.Excel;

public class ExpandableListAdapter extends BaseExpandableListAdapter {

    private Context context;
    private List<Candidat> candidats = new ArrayList<>();
    private ArrayList<String> personalReps = new ArrayList<>();
    private final int CANDIDAT_NON_REPONDU = 0;
    private final int CANDIDAT_MAL_REPONDU = 1;
    private final int CANDIDAT_BIEN_REPONDU = 2;

    private  int correctRep = 0;
    private LinkedHashMap<com.polytest.polymont.polytest.Data.Candidat, ArrayList<Phrase>> listChild = new LinkedHashMap<>();
    private SimpleDateFormat formatDateCAL = new SimpleDateFormat("dd/MM/yy HH:mm");
    private final SimpleDateFormat dateFormatTS = new SimpleDateFormat("mm:ss");


    public ExpandableListAdapter(Context context, ArrayList<Candidat> candidats , LinkedHashMap<com.polytest.polymont.polytest.Data.Candidat, ArrayList<Phrase>> listChild, ArrayList<String> personalReps) {
        this.context = context;
        this.candidats = candidats;
        this.listChild = listChild;
        this.personalReps = personalReps;
    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {

        return this.listChild.get(this.candidats.get(groupPosition)).get(childPosititon);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @SuppressLint("InflateParams")
    @Override
    public View getChildView(int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {


        LayoutInflater infalInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = infalInflater.inflate(R.layout.row_admin_item, null);

        TextView enonceQuestion = (TextView) convertView.findViewById(R.id.admin_enonce_question);
        LinearLayout layoutReponse = (LinearLayout) convertView.findViewById(R.id.layout_answer);

        //si child position vaut 0 alors la phrase trouvee est une question
        if(childPosition%5 == 0){

            layoutReponse.removeAllViews();


            Question question = (Question) getChild(groupPosition, childPosition);
            enonceQuestion.setText(question.getEnonce());

            layoutReponse.setVisibility(View.GONE);
        }
        //sinon la phrase est une reponse
        else{

            Reponse reponse = (Reponse) getChild(groupPosition, childPosition);
            TextView tvReponse = new TextView(context);
            tvReponse.setTextSize(18);
            tvReponse.setBackgroundColor(ResourcesCompat.getColor(context.getResources(), R.color.colorGrisPolymont,null));
            tvReponse.setGravity(Gravity.CENTER);
            tvReponse.setText(reponse.getEnonce());

            if(!personalReps.get(childPosition/5).equals("false")){
                correctRep = verifyRep(reponse, personalReps.get(childPosition/5), childPosition);
            }
            else{
                correctRep = CANDIDAT_NON_REPONDU;
            }

            if(reponse.isCorrect()){
                tvReponse.setCompoundDrawablesWithIntrinsicBounds(0,0,R.mipmap.tick30,0);
            }
            else{
                tvReponse.setCompoundDrawablesWithIntrinsicBounds(0,0,R.mipmap.cross30,0);
            }

            switch (correctRep){
                case CANDIDAT_BIEN_REPONDU:
                    tvReponse.setBackgroundColor(ResourcesCompat.getColor(context.getResources(), R.color.correctAnswer, null));
                    break;

                case CANDIDAT_MAL_REPONDU:
                    tvReponse.setBackgroundColor(ResourcesCompat.getColor(context.getResources(), R.color.wrongAnswer, null));
                    break;

                case CANDIDAT_NON_REPONDU:
                    break;
            }

            layoutReponse.addView(tvReponse);

            enonceQuestion.setVisibility(View.GONE);
        }



        return convertView;
    }

    // reponse : reponse courrante
    // candidatRep : reponse du candidat sous forme (1 2 3 4)
    // childPosition : position phrase [0,99]
    private int verifyRep(Reponse response, String candidatRep, int childPosition) {
        int candidatReponse = CANDIDAT_NON_REPONDU;
        String[] repsTab = candidatRep.trim().split("\\s+");

        for (String aRepsTab : repsTab) {
            //crtRep appartient a [1,4]
            if (!aRepsTab.equals("")) {
                int crtRep = Integer.valueOf(aRepsTab);
                if ((childPosition % 5) == crtRep) {
                    candidatReponse = response.isCorrect() ? CANDIDAT_BIEN_REPONDU : CANDIDAT_MAL_REPONDU;
                    break;
                }
            }
        }

        return candidatReponse;
    }

    @Override
    public int getChildrenCount(int groupPosition) {

        return this.listChild.get(this.candidats.get(groupPosition)).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this.candidats.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this.candidats.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @SuppressLint("InflateParams")
    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this.context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.row_admin, null);
        }


        TextView idCandidat = (TextView)convertView.findViewById(R.id.id_candidat);
        TextView prenomNom = (TextView)convertView.findViewById(R.id.name_candidat);
        TextView scoreCandidat = (TextView)convertView.findViewById(R.id.score_candidat);
        TextView dataCandidat = (TextView) convertView.findViewById(R.id.date_candidat);
        TextView durationCandidat = (TextView) convertView.findViewById(R.id.duration_candidat);
        TextView testDoneCandidat = (TextView) convertView.findViewById(R.id.test_done_candidat);
        TextView difficulte = (TextView) convertView.findViewById(R.id.level_row);

        Candidat candidatCurrent =  (Candidat) getGroup(groupPosition);

        Calendar calendar =candidatCurrent.getDateTest();
        String dateTest = formatDateCAL.format(calendar.getTime());
        String durationTest = dateFormatTS.format(candidatCurrent.getDurationTest().getTime());


        idCandidat.setText(String.valueOf(candidatCurrent.getId()));
        prenomNom.setText(candidatCurrent.getNom());
        scoreCandidat.setText(String.valueOf(candidatCurrent.getScore())+context.getString(R.string.lesurvingt));
        dataCandidat.setText(dateTest);
        durationCandidat.setText(durationTest);
        testDoneCandidat.setText(candidatCurrent.getQuizz().getTestName());
        if(candidatCurrent.getQuizz().getTestLevel().equals(Excel.NIVEAU_JUNIOR)){
            difficulte.setText(R.string.junior);
        }
        else{
            difficulte.setText(R.string.expert);
        }

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}