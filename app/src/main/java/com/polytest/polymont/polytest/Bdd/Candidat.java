package com.polytest.polymont.polytest.Bdd;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.polytest.polymont.polytest.Data.Question;
import com.polytest.polymont.polytest.Data.Quizz;
import com.polytest.polymont.polytest.Data.Reponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;


/**
 * Created by Zaccharie.BOUVY on 17/01/2017.
 */

public class Candidat{

    private static final String TABLE_CANDIDAT = "Candidat";
    private static final String COL_ID = "id";
    private static final String COL_NAME = "name";
    private static final String COL_SCORE = "score";
    private static final String COL_DATE_TEST = "date";
    private static final String COL_DURATION_TEST = "duration";
    private static final String COL_TEST_DONE = "test";
    private static final String COL_TEST_LEVEL = "level";
    private static final String COL_FK_QUIZZ = "leQuizz";

    private static final SimpleDateFormat dateFormatCAL = new SimpleDateFormat("yyyy-MM-dd HH:mm");
    private static final SimpleDateFormat dateFormatTS = new SimpleDateFormat("mm:ss");


    private SQLiteDatabase bdd;
    private BaseSQLite mBaseSQLite;
    private Context context;


    private static final int VERSION_BDD = 1;
    private static final String NOM_BDD = "polytest.db";

    public SQLiteDatabase getBDD(){
        return bdd;
    }


    public Candidat(Context context) {
        mBaseSQLite = new BaseSQLite(context, NOM_BDD, null, VERSION_BDD);
        this.context = context;
    }

    public ContentValues jsonToContentValues(JSONObject json) {
        ContentValues values = new ContentValues();
        values.put(COL_FK_QUIZZ, json.optString("leQuizz"));
        return values;
    }

    public void insertCandidat(com.polytest.polymont.polytest.Data.Candidat candidat) {
        this.open();
        ContentValues values = new ContentValues();
        values.put(COL_NAME, candidat.getNom());
        values.put(COL_SCORE, candidat.getScore());
        values.put(COL_DATE_TEST, dateFormatCAL.format(Calendar.getInstance().getTime()));
        values.put(COL_DURATION_TEST, dateFormatTS.format(candidat.getDurationTestTS()));
        values.put(COL_FK_QUIZZ, String.valueOf(candidat.getQuizz().getEntireMapJson(candidat)));
        values.put(COL_TEST_DONE, candidat.getQuizz().getTestName());
        values.put(COL_TEST_LEVEL, candidat.getQuizz().getTestLevel());

        bdd.insert(TABLE_CANDIDAT, null, values);
        this.close();
    }


    public ArrayList<com.polytest.polymont.polytest.Data.Candidat> getAllCandidat(){
        this.open();
        ArrayList<com.polytest.polymont.polytest.Data.Candidat> candidats = new ArrayList<>();

        //Cursor c = bdd.rawQuery("select * from " + TABLE_EVENEMENT + " Where "+ COL_FK_DEVICEID_EVENT + " = '"+device_id+"'", null);
        Cursor c = bdd.rawQuery("select * from " + TABLE_CANDIDAT, null);
        for (c.moveToFirst(); !c.isAfterLast(); c.moveToNext()){

            com.polytest.polymont.polytest.Data.Candidat candidat = new com.polytest.polymont.polytest.Data.Candidat();

            Calendar cal = Calendar.getInstance();
            Calendar durationCal = Calendar.getInstance();

            try{
                cal.setTime(dateFormatCAL.parse(c.getString(c.getColumnIndex(COL_DATE_TEST))));
                durationCal.setTime((dateFormatTS.parse(c.getString(c.getColumnIndex(COL_DURATION_TEST)))));
            }catch (Exception e) {
                e.printStackTrace();
            }

            candidat.setId(c.getInt(c.getColumnIndex(COL_ID)));
            candidat.setScore(c.getInt(c.getColumnIndex(COL_SCORE)));
            candidat.setNom(c.getString(c.getColumnIndex(COL_NAME)));

            try {
                Quizz quizz = new Quizz();
                JSONArray leQuizzJSON = new JSONArray(c.getString(c.getColumnIndex(COL_FK_QUIZZ)));

                for(int i = 0; i < leQuizzJSON.length();i++){

                    JSONObject phrase = leQuizzJSON.getJSONObject(i);
                    String enonceQ = phrase.getString("question");
                    String personalReps = phrase.getString("personal-response");
                    JSONArray reps = phrase.getJSONArray("reps");

                    candidat.addPersonalRep(personalReps);

                    Question question = new Question(enonceQ);

                    ArrayList<Reponse> repsList = new ArrayList<>();


                    for(int j=0;j<reps.length();j++){

                        JSONObject reponse = reps.getJSONObject(j);
                        String enonceR = reponse.getString("enonce");
                        String correct = reponse.getString("correct");

                        Reponse r = new Reponse(enonceR,Boolean.valueOf(correct));
                        repsList.add(r);
                    }

                    quizz.addQuestion(question, repsList);

                }

                quizz.setTestName(c.getString(c.getColumnIndex(COL_TEST_DONE)));
                quizz.setTestLevel(c.getString(c.getColumnIndex(COL_TEST_LEVEL)));
                candidat.setQuizz(quizz);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            candidat.setDateTest(cal);
            candidat.setDurationTest(durationCal);

            candidats.add(candidat);

        }
        this.close();
        return candidats;

    }

    public void resetDataBase()
    {
        this.open();
        bdd.execSQL("UPDATE SQLITE_SEQUENCE SET seq = 0 WHERE name=\""+TABLE_CANDIDAT+"\"");
        bdd.delete(TABLE_CANDIDAT,null,null);
        bdd.execSQL("delete from "+ TABLE_CANDIDAT);
        this.close();
    }

    public void open(){
        bdd = mBaseSQLite.getWritableDatabase();
    }
    private void close(){
        bdd.close();
    }

}

