package com.polytest.polymont.polytest.Data;

import com.polytest.polymont.polytest.Util.JSONFormat;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedList;

public class Quizz implements Serializable {

    private LinkedHashMap<Question, ArrayList<Reponse>> quizzMap = new LinkedHashMap<>();
    private String testName;
    private String testLevel;

    public Quizz(){}

    public Quizz(LinkedHashMap<Question, ArrayList<Reponse>> quizzMap, String testLevel, String testName) {
        this.quizzMap = quizzMap;
        this.testLevel = testLevel;
        this.testName = testName;
    }

    public Quizz(LinkedHashMap <Question, ArrayList<Reponse>> quizzMap, String testName){
        this.quizzMap = quizzMap;
        this.testName = testName;
    }

    public Quizz(LinkedHashMap <Question, ArrayList<Reponse>> quizzMap){
        this.quizzMap = quizzMap;
    }

    public JSONArray getEntireMapJson(Candidat candidat){

        JSONArray jsonArray = new JSONArray();
        JSONFormat jsonFormat = new JSONFormat(candidat);

        try {
            jsonArray = jsonFormat.createJSON();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonArray;
    }

    public Question getQuestion(int position){

        return new ArrayList<> (quizzMap.keySet()).get(position);
    }

    public ArrayList<Reponse> getReponses(int position){

        return new  ArrayList<> (quizzMap.values()).get(position);
    }

    public void addQuestion(Question question, ArrayList<Reponse> reponses){
        quizzMap.put(question, reponses);
    }


    public int size(){
        return quizzMap.size();
    }

    public void clear(){ quizzMap.clear();}

    public LinkedHashMap<Question, ArrayList<Reponse>> getQuizzMap() {
        return quizzMap;
    }

    public void setQuizzMap(LinkedHashMap<Question, ArrayList<Reponse>> quizzMap) {
        this.quizzMap = quizzMap;
    }

    public JSONArray getArrayOfRepsIntoJson(int i){

        ArrayList<Reponse> reponses = getReponses(i);

        JSONArray jsonArray = new JSONArray();
        for(Reponse r : reponses){
            jsonArray.put(r.getJSONObject());
        }

        return jsonArray;
    }

    public void setTestName(String testName){
        this.testName = testName;
    }

    public String getTestName() {
        return testName;
    }

    public String getTestLevel() {
        return testLevel;
    }

    public void setTestLevel(String testLevel) {
        this.testLevel = testLevel;
    }
}
